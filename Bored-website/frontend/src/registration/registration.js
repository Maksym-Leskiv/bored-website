import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import cn from "classnames";
import Particles from "react-particles-js";
import params from "../particle-js";
import AuthenticationService from "../services/authentication";
import "./registration.css";
import {
  usernameRegExp,
  emailRegExp,
  passwordRegExp
} from "../regular-expressions";

class Registration extends Component {
  static initialValues = {
    username: "",
    email: "",
    password: "",
    passwordConfirm: ""
  };
  static schema = Yup.object().shape({
    username: Yup.string().trim().matches(usernameRegExp).required(),
    email: Yup.string().trim().matches(emailRegExp).required(),
    password: Yup.string().trim().matches(passwordRegExp).required(),
    passwordConfirm: Yup.string()
      .trim()
      .matches(passwordRegExp)
      .required()
      .oneOf([Yup.ref("password"), null])
  });

  onUserSubmitRegistration = async ({
    username,
    email,
    password,
    passwordConfirm
  }) => {
   await AuthenticationService.registerUser({
      username,
      email,
      password,
      passwordConfirm
    });
	this.redirectionToLogin();
  };

  redirectionToLogin = () => this.props.history.push("/login");

  render() {
    return (
      <div className="register-page">
        <Particles params={params} />
        <Formik
          initialValues={Registration.initialValues}
          onSubmit={this.onUserSubmitRegistration}
          validationSchema={Registration.schema}
        >
          {({ values, errors, touched, handleChange, handleSubmit }) => {
            const isInputInvalid = valueKey =>
              Boolean(touched[valueKey] && errors && errors[valueKey]);
            return (
              <div className="register-form">
                <h2>register</h2>
                <form onSubmit={handleSubmit}>
                  <div className="form-group">
                    <input
                      type="text"
                      name="username"
                      value={values.username}
                      className={cn("form-control", {
                        borderRed: isInputInvalid("username")
                      })}
                      placeholder="Username"
                      onChange={handleChange}
                    />
                    {isInputInvalid("username") && (
                      <span className="error">Invalid username</span>
                    )}
                  </div>

                  <div className="form-group">
                    <input
                      type="text"
                      name="email"
                      value={values.email}
                      className={cn("form-control", {
                        borderRed: isInputInvalid("email")
                      })}
                      placeholder="Email"
                      onChange={handleChange}
                    />
                    {isInputInvalid("email") && (
                      <span className="error">Invalid email</span>
                    )}
                  </div>

                  <div className="form-group">
                    <input
                      type="password"
                      name="password"
                      value={values.password}
                      className={cn("form-control", {
                        borderRed: isInputInvalid("password")
                      })}
                      placeholder="Password"
                      onChange={handleChange}
                    />
                    {isInputInvalid("password") && (
                      <span className="error">
                        Password must contain at least one:Upper - Lower letter,
                        Number, and be 8 symbols long
                      </span>
                    )}
                  </div>

                  <div className="form-group">
                    <input
                      type="password"
                      name="passwordConfirm"
                      value={values.passwordConfirm}
                      className={cn("form-control", {
                        borderRed: isInputInvalid("passwordConfirm")
                      })}
                      placeholder="Confirm password"
                      onChange={handleChange}
                    />
                    {isInputInvalid("passwordConfirm") && (
                      <span className="error">Password must match</span>
                    )}
                  </div>

                  <button type="submit" className="submit-btn">
                    fire to register
                  </button>
                  <button
                    onClick={this.redirectionToLogin}
                    className="submit-btn"
                  >
                    have a account? log in here
                  </button>
                </form>
              </div>
            );
          }}
        </Formik>
      </div>
    );
  }
}
export default withRouter(Registration);
