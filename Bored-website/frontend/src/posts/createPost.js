import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import cn from "classnames";
import PostService from "../services/postService";
import "bootstrap/dist/css/bootstrap.min.css";
import "./createPost.css";
import Navbar from "../navbar/navbar";

class CreatePost extends Component {
	
	state = {
		selectedFile: ""
	}
	
	static initialValues = {
		title: "",
		description: "",
		category: "other",
		meetingTime: "",
	};

	static schema = Yup.object().shape({
		title: Yup.string().trim().min(2).required(),
		description: Yup.string().trim().min(20).required(),
		category: Yup.string(),
		meetingTime: Yup.string().trim().required()
	});

	onCreatePost = async ({
		title,
		description,
		category,
		meetingTime,
	}) => {
		const username = sessionStorage.getItem("username");
		const { selectedFile } = this.state;
		await PostService.createPost(
			{
				title,
				description,
				category,
				meetingTime,
				username,
				encodedImage: selectedFile
			}
		);
		this.redirectionToMain();
	};

	redirectionToMain = () => this.props.history.push("/main");

	onFileChange = event => {
		console.log(event.target.files[0]);
		this.setState({ selectedFile: event.target.files[0] });
		this.getBase64(event.target.files[0]).then(result => {
			const afterSplit = result.split("base64,")[1];
			this.setState({selectedFile: afterSplit})
			});
		
	};


	getBase64 = (file) => new Promise(function(resolve, reject) {
		let reader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => resolve(reader.result)
		reader.onerror = (error) => reject('Error: ', error);
	})

	render() {
		return (
			<>
				<Navbar />
				<div className="create-post">
					<Formik
						initialValues={CreatePost.initialValues}
						onSubmit={this.onCreatePost}
						validationSchema={CreatePost.schema}
					>
						{({
							values,
							errors,
							touched,
							handleChange,
							handleSubmit,
							setFieldValue
						}) => {
							const isInputInvalid = valueKey =>
								Boolean(touched[valueKey] && errors && errors[valueKey]);

							return (
								<div className="create-post">
									<h4>Create Post</h4>
									<form onSubmit={handleSubmit}>
										<div className="form-group">
											<input
												type="text"
												name="title"
												value={values.title}
												className={cn("form-control", {
													borderRed: isInputInvalid("title")
												})}
												placeholder="title"
												onChange={handleChange}
											/>
											{isInputInvalid("title") && (
												<span className="error">Invalid title</span>
											)}
										</div>

										<div className="form-group">
											<input
												type="text"
												name="description"
												value={values.description}
												className={cn("form-control", {
													borderRed: isInputInvalid("description")
												})}
												placeholder="description"
												onChange={handleChange}
											/>
											{isInputInvalid("description") && (
												<span className="error">Invalid description</span>
											)}
										</div>

										<div className="form-group">
											<input
												type="datetime-local"
												className={cn("form-control", {
													borderRed: isInputInvalid("meetingTime")
												})}
												value={values.meetingTime}
												name="meetingTime"
												onChange={e =>
													setFieldValue(e.target.name, e.target.value)
												}
												min={new Date().toISOString().slice(0, 16)}
											/>
											{isInputInvalid("meetingTime") && (
												<span className="error">Invalid meeting time</span>
											)}
										</div>

										<select
											name="category"
											defaultValue="other"
											className="form-select"
											onChange={e =>
												setFieldValue(e.target.name, e.target.value)
											}
										>
											<option value="activity">activity</option>
											<option value="culture">culture</option>
											<option value="board games">board games</option>
											<option value="other">other</option>
										</select>

										<label className="custom-file-upload">
											Upload photo
                      						<input
												name="postEncodedImage"
												type="file"
												onChange={this.onFileChange}
											/>
										</label >

										<button

											className="submit-btn"
											type="submit"
										>
											fire to create post
                    					</button>
									</form>
								</div>
							);
						}}
					</Formik>
				</div>
			</>
		);
	}
}

export default withRouter(CreatePost);
