import React, { Component } from "react";
import "./myPosts.css";
import Navbar from "../navbar/navbar";
import PostService from "../services/postService";
import PostCard from "../post-card";

class MyPosts extends Component {
 
	state = {
		allPosts: []
	};

	async componentDidMount() {
		await this.fetchAndSetPosts();
	}
	
	fetchAndSetPosts = async() => {
		const allPosts = await PostService.getAllPostsByUsername();
		this.setState({ allPosts: allPosts.data || []})
	}
	
	deletePost = async (id) =>{ 
	 await PostService.deletePostById(id);
	 await this.fetchAndSetPosts();
	}
	
	updatePostTitleDescription = async (id, description, title) =>{
	 await PostService.updatePostTitleDescription(id, description,title);
	 await this.fetchAndSetPosts();	
	}

 render() {
	const { allPosts } = this.state;
    return (
      <>
        <Navbar />
		<div className="container">
        {allPosts && (
          <div className="myPostWrapper">
            {allPosts.map(post => (
              <PostCard 
					key={post.id} 
					post={post}
					deletePost={this.deletePost} 
					updatePostTitleDescription={this.updatePostTitleDescription}
					/>
            ))}
          </div>
        )}
		</div>
      </>
    );
  }
}

export default MyPosts;
