import React, { Component } from "react";
import "./card.css";

class Card extends Component {
	state = { commentText: "" }
	render() {
		const { post, addComment } = this.props;

		return (
			<div className="card">

				<img
					className="card-img-top"
					src={`data:image/jpg;base64,${post.encodedImage}`}
				></img>
				<div className="card-body">
					<h5 className="card-title">title: {post.title}</h5>
					<p className="card-text">description: {post.description}</p>
					<p className="card-text">time: {post.time}</p>
					<p className="card-text">category: {post.category}</p>
					<p className="card-text">{post.username}</p>
				</div>

				{Boolean(post.comments.length) && <div className="comment-view">
				<hr/>
					{post.comments.map(comment => (
						<>
							<p>comment: {comment.text}</p>
							<p>author: {comment.authorName}</p>
							<hr/>
						</>
					))}

				</div>}

				<div className="comment-create">
					<input name="postId" type="hidden" value="{post.id}" />
					<input name="userName" type="hidden" value="${user.username}" />
					<input onChange={e => this.setState({ commentText: e.target.value })} name="commentText" type="text" placeholder="comment" />
					<button onClick={() => addComment(post.id, this.state.commentText)} className="submit-btn">
						submit
          </button>
				</div>
			</div>
		);
	}
}

export default Card;
