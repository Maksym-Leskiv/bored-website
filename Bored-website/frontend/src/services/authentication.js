import { Component } from "react";
import axios from "axios";
import { baseUrl } from "./constants";

class AuthenticationService extends Component {
	async loginUser(config = {}) {
		try {
			const response = await axios.post(`${baseUrl}/login`, config);
			if (response && response.status === 200) {
				let token = response.headers.authorization;
				axios.defaults.headers["Authorization"] = token;
			}
			return response;
		} catch (error) {
			console.log(error);
		}
	}

	async registerUser(config = {}) {
		return await axios.post(`${baseUrl}/registration`, config);
	}

	async deleteUser(id) {
		return await axios.post(`${baseUrl}/adminPanel/deleteUser/${id}`);
	}

	logoutUser() {
		localStorage.removeItem("jwtToken");
		sessionStorage.removeItem("username");
		sessionStorage.removeItem("role");
	}
}

const instance = new AuthenticationService();

export default instance;
