import { Component } from "react";
import axios from "axios";
import { baseUrl } from "./constants";

class AdminService extends Component {
	async deleteUser(id) {
		return await axios.delete(`${baseUrl}/adminPanel/deleteUser/${id}`, {
				headers: {
					Authorization: localStorage.getItem("jwtToken")}
				});
	}

	async changeUserRole(id) {
		return await axios.put(`${baseUrl}/adminPanel/changeUserRole/${id}`, {} ,{
			headers: {
				Authorization: localStorage.getItem("jwtToken")
			}
		});
	}

	async getAllUsers() {
		try {
			return await axios.get(`${baseUrl}/adminPanel`, {
				headers: {
					Authorization: localStorage.getItem("jwtToken")
				}
			});
		} catch (error) {
			console.log(error);
			return [];
		}
	}
	
	async getUserByUsername() {
		try {
			const username = sessionStorage.getItem("username");
			const response =  await axios.get(`${baseUrl}/adminPanel/${username}`, {
				headers: {
					Authorization: localStorage.getItem("jwtToken")
				}
			});
			return response.data;
		} catch (error) {
			console.log(error);
		}
	}
}

const instance = new AdminService();

export default instance;
