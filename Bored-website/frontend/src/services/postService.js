import { Component } from "react";
import axios from "axios";
import { baseUrl } from "./constants";

class PostService extends Component {

	async createPost(config = {}) {
		
		const data = {
			"title": config.title,
			"description": config.description,
			"category": config.category,
			"time": config.meetingTime,
			"username": config.username,
			"encodedImage": config.encodedImage
		}
		return await axios.post(`${baseUrl}/createPost`, data, {
			headers: {
				Authorization: localStorage.getItem("jwtToken")
			}
		});
	}

	async updatePostTitleDescription(id, title, description ) {
		const data = {

			"title": title,
			"description": description
		}
		return await axios.put(`${baseUrl}/updateTitleDescription/${id}`, data, {
			headers: {
				Authorization: localStorage.getItem("jwtToken")
			}
		})
	}
	
	async getAllPosts() {
		try {
			const resp = await axios.get(`${baseUrl}/main`, {
				headers: {
					Authorization: localStorage.getItem("jwtToken")
				}
			});
			return resp;
		} catch (error) {
			console.log(error);
			return [];
		}
	}

	async getAllPostsByUsername() {
		try {
			const username = sessionStorage.getItem("username");
			const resp = await axios.get(`${baseUrl}/myPosts/${username}`, {
				headers: {
					Authorization: localStorage.getItem("jwtToken")
				}
			});
			return resp
		} catch (error) {
			console.log(error);
			return [];
		}
	}

	async deletePostById(id) {
		return await axios.delete(`${baseUrl}/deletePost/${id}`, {
			headers: {
				Authorization: localStorage.getItem("jwtToken")
			}
		});
	}

	async addComment(id, authorName, text) {
		const data = {
			"text": text,
			"authorName": authorName,
		}
		return await axios.post(`${baseUrl}/commentPost/${id}`, data, {
			headers: {
				Authorization: localStorage.getItem("jwtToken")
			}
		})
	}
	
	
}

const instance = new PostService();

export default instance;
