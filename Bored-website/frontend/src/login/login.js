import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import { Formik } from "formik";
import * as Yup from "yup";
import cn from "classnames";
import Particles from "react-particles-js";
import params from "../particle-js";
import AuthenticationService from "../services/authentication";
import { usernameRegExp } from "../regular-expressions";
import "./login.css";
import AdminService from "../services/adminService";

class Login extends Component {
	static initialValues = {
		username: "",
		password: ""
	};

	state = {
		error: ""
	};

	static schema = Yup.object().shape({
		username: Yup.string().trim().matches(usernameRegExp).required(),
		password: Yup.string().trim().required()
	});

	onUserSubmit = async ({ username, password }) => {
		const response = await AuthenticationService.loginUser({
			username,
			password
		});
		if (response && response.status === 200) {
			let token = response.headers.authorization;
			console.warn("token", token);
			localStorage.setItem("jwtToken", token);
			const userData = JSON.parse(response.config.data).username;
			sessionStorage.setItem("username", userData);
			const { role } = await AdminService.getUserByUsername();
			sessionStorage.setItem("role", role);
			this.props.history.push("/main");
		} else {
			this.setState({ error: "Wrong credentials" });
		}
	};

	redirectionToRegister = () => this.props.history.push("/registration");

	render() {
		return (
			<div className="login-page">
				<Particles params={params} />

				<Formik
					initialValues={Login.initialValues}
					onSubmit={this.onUserSubmit}
					validationSchema={Login.schema}
				>
					{({ values, errors, touched, handleChange, handleSubmit }) => {
						const isInputInvalid = valueKey =>
							Boolean(touched[valueKey] && errors && errors[valueKey]);
						const { error } = this.state;
						return (
							<div className="login-form">
								<h1>Login</h1>
								<form onSubmit={handleSubmit}>
									<div className="form-group">
										<input
											type="text"
											name="username"
											value={values.username}
											className={cn("form-control", {
												borderRed: isInputInvalid("username")
											})}
											placeholder="Username"
											onChange={handleChange}
										/>
										{isInputInvalid("username") && (
											<span className="error">Invalid username</span>
										)}
									</div>

									<div className="form-group">
										<input
											type="password"
											name="password"
											value={values.password}
											className={cn("form-control", {
												borderRed: isInputInvalid("password")
											})}
											placeholder="Password"
											onChange={handleChange}
										/>
										{isInputInvalid("password") && (
											<span className="error">Invalid password</span>
										)}
									</div>

									<button className="submit-btn" type="submit">
										fire to login
                  </button>
									{error && <span className="error">{error}</span>}
									<button
										onClick={this.redirectionToRegister}
										className="submit-btn"
									>
										don't have a account? register here
                  </button>
								</form>
							</div>
						);
					}}
				</Formik>
			</div>
		);
	}
}

export default withRouter(Login);
