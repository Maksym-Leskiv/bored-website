import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import Login from './login/login'
import Registration from './registration/registration'
import CreatePost from './posts/createPost'
import MyPosts from './posts/myPosts'
import HomePage from "./homepage";
import Main from "./main/main";
import AdminPanel from "./admin/admin";
import ProtectedRoute from "./protected-route"

const App = () => (
	<Router>
		<Switch>
			<Route path="/login" component={Login}/>
				
			<Route path="/registration" component={Registration}/>

			<ProtectedRoute path="/create-post" component={CreatePost} />

			<ProtectedRoute path="/main" component={Main} />

			<ProtectedRoute path="/admin-panel" component={AdminPanel} isAdminPanel />

			<ProtectedRoute path="/my-posts" component={MyPosts} />
		
			<Route path="/" component={HomePage}/>
		</Switch>
	</Router>
);

export default App;
