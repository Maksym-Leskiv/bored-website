export const usernameRegExp =  /^[A-Za-z]+$/;
export const emailRegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
export const passwordRegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}$/;
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';