import React, { Component } from "react";
import "./myPosts.css";



class PostCard extends Component {

	state = {
		description: "",
		title: ""
	}
	
	updatePost= async(id) => {
		const{title, description} = this.state;
		this.props.updatePostTitleDescription(id, description, title);
		this.setState({
		description: "",
		title: ""
	})
	}

	render() {
		const { post, deletePost, updatePostTitleDescription } = this.props;

		return (
			<div className="card">
				<img
					className="card-img-top"
					src={`data:image/jpg;base64,${post.encodedImage}`}></img>
				<div className="card-body">
					<h5 className="card-title">title: {post.title}</h5>
					<p className="card-text">description: {post.description}</p>
					<p className="card-text">time: {post.time}</p>
					<p className="card-text">category: {post.category}</p>
					<p className="card-text">{post.username}</p>
				</div>

				<div>
					<button onClick={() => deletePost(post.id)} className="submit-btn">delete</button>
				</div>

				<div>
					<input
						type="hidden"
						id="postId"
						name="postId"
						value="{this.props.post.id}"
					/>
					<input
						type="text"
						onChange={e => this.setState({title:e.target.value})}
						name="postTitleUpdate"
						placeholder="update your post title"
						value={this.state.title}
					/>
					<br />
					<input
						type="text"
						onChange={e => this.setState({description:e.target.value})}
						name="postDescriptionUpdate"
						placeholder="update your post description"
						value={this.state.description}
					/>
					<br />

					<button onClick={() => this.updatePost(post.id)} className="submit-btn" type="submit">
						update
         		    </button>
				</div>
			</div>
		);
	}
}

export default PostCard;
