import React from "react";
import { Redirect, Route } from "react-router-dom";

function ProtectedRoute({ component: Component, isAdminPanel, ...restOfProps }) {
	const isAuthenticated = sessionStorage.getItem("username");
	const userRole = sessionStorage.getItem("role");
	console.log(isAdminPanel)

	const getRoute = () => {
		return isAdminPanel ? <Route
			{...restOfProps}
			render={(props) =>
				isAuthenticated && userRole === "ROLE_ADMIN" ? <Component {...props} /> : <Redirect to="/main" />
			}
		/>
			: <Route
				{...restOfProps}
				render={(props) =>
					isAuthenticated ? <Component {...props} /> : <Redirect to="/login" />
				}
			/>
	}
	return getRoute();
}

export default ProtectedRoute;