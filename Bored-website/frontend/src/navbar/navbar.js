import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../navbar/navbar.css";
import { withRouter } from "react-router-dom";
import  AuthenticationService  from "../services/authentication";

class Navbar extends Component {
state = {
	userRole: ""
}

    componentDidMount(){
	const userRole = sessionStorage.getItem("role");
	this.setState({userRole});
}

  logout = async () => {
   await AuthenticationService.logoutUser();
   this.props.history.push("/");	
};

  render() {
    return (
      <div id="navbar-place">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <h4>Bored</h4>
            <button
              className="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNavAltMarkup"
              aria-controls="navbarNavAltMarkup"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
              <div className="navbar-nav">
                <Link to="/main" className="menu">
                  Main
                </Link>
                <Link to="/create-post" className="menu">
                  Add Post
                </Link>
                <Link to="/my-posts" className="menu">
                  My Posts
                </Link>
               { this.state.userRole === "ROLE_ADMIN" && <Link to="/admin-panel" className="menu">
                  Admin Panel
                </Link>}
				
                <Link className="logout" onClick={this.logout}>
                  Log out
                </Link>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Navbar);
