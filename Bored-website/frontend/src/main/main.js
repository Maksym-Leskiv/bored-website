import React, { Component } from "react";
import "./main.css";
import Navbar from "../navbar/navbar";
import PostService from "../services/postService";
import Card from "../card";

class Main extends Component {
	state = {
		allPosts: []
	};

	componentDidMount() {
		this.fetchAllPosts();
	}

	addComment = async (id,commentText) => {
		const username = sessionStorage.getItem("username");
		await PostService.addComment(id, username, commentText);
		await this.fetchAllPosts();
	}
	
	fetchAllPosts = async () => {
		const allPosts = await PostService.getAllPosts();
		this.setState({ allPosts: allPosts.data || [] });
	}

	render() {
		const { allPosts } = this.state;
		return (
			<>
				<Navbar />
				<div className="container">
					{allPosts && (
						<div className="postWrapper">
							{allPosts.map(post => (
								<Card
									key={post.id}
									post={post}
									addComment={this.addComment} />
							))}
						</div>
					)}
				</div>
			</>
		);
	}
}

export default Main;
