import { useHistory } from "react-router";
import "./homepage.css";

function HomePage() {
	const history = useHistory();
	const redirectionToLogin = () => {
		history.push('/login')
	}
	return (
		<div className="container-home">
			<div id="logoDiv">
				<h1>Bored?</h1>
				<button onClick={redirectionToLogin}>Sing in</button>
			</div>
		</div>
	);
}


export default HomePage;