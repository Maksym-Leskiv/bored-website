import AuthenticationService from '../services' 

function Logout() {
	const onUserSubmit = (event) => {
		event.preventDefault()
		AuthenticationService.logoutUser({})
	}
	return (
		<div>
			<h5>login</h5>

			<form onSubmit={onUserSubmit}>
				<button type="submit">fire to logout</button>
			</form>
		</div>
	);
}

export default Logout;
