import React, { Component } from "react";
import "./admin.css";
import Navbar from "../navbar/navbar";
import AdminService from "../services/adminService";
import CardUser from "../user-card/";

class AdminPanel extends Component {
	state = {
		allUsers: []
	};

	async componentDidMount() {
		await this.fetchAndSetUsers();
	}

	fetchAndSetUsers = async() => {
		const allUsers = await AdminService.getAllUsers();
		this.setState({ allUsers: allUsers.data || [] });
	}

	deleteUser = async (id) => {
	  await	AdminService.deleteUser(id);
	  await this.fetchAndSetUsers();
	}

	changeUserRole = async (id) => {
		await AdminService.changeUserRole(id);
		await this.fetchAndSetUsers();
	}

	render() {
		const { allUsers } = this.state;
		return (
			<>
				<Navbar />
				{allUsers && (
					<div className="userCardWrapper">
						{allUsers.map(user => (
							<CardUser 
								key={user.id} 
								user={user}
								deleteUser={this.deleteUser} 
								changeUserRole={this.changeUserRole} />
						))}
					</div>
				)}
			</>
		);
	}
}
export default AdminPanel;
