const config = {
  particles: {
    number: {
      value: 50,
      density: {
        enable: true,
        value_area: 1500
      }
    },
    line_linked: {
      enable: true,
      opacity: 0.3,
	  color: "#000000"
    },
    move: {
      direction: "top",
      speed: 0.5
    },
    size: {
      value: 3
    },
    "color": {
            "value": "#000000"
        },
    opacity: {
      anim: {
        enable: true,
        speed: 2,
        opacity_min: 0.1	
      }
    }
  },
  interactivity: {
    events: {
      onclick: {
        enable: true,
        mode: "push"
      }
    },
    modes: {
      push: {
        particles_nb: 1
      }
    }
  },
  retina_detect: true
};

export default config;
