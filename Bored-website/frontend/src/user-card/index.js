import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import "./userCard.css";

class UserCard extends Component {

	render() {
		const { user, deleteUser, changeUserRole } = this.props;

		return (
			<div className="card">
				<div className="card-body">
					<h5 className="card-username">{user.username}</h5>
					<p className="card-user-role">role: {user.role}</p>
					<p className="card-user-id">id: {user.id}</p>
				</div>
				<div><button onClick={() => changeUserRole(user.id)} className="submit-btn">change role</button></div>
				<div><button onClick={() => deleteUser(user.id)} className="submit-btn">delete</button></div>
			</div>
		);
	}
}

export default withRouter(UserCard);