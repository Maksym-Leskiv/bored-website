  
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="stylesheet" type="text/css" href="css/main.css">
<title>Bored Main</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
 
 	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <p class="nav-item nav-link"> Welcome ${user.username}</p>
      <a class="nav-item nav-link" href="main">Main</a>
      <a class="nav-item nav-link" href="createPost">Add Post</a>
      <a class="nav-item nav-link" href="myPosts">Show My Posts</a>
      <sec:authorize access="hasRole('ROLE_ADMIN')">
      <a class="nav-item nav-link" href="adminPanel">Admin Panel</a>
      </sec:authorize>
      <a class="nav-item nav-link" href="#" onclick="document.forms['logoutForm'].submit()">Exit</a>
    </div>
  </div>
</nav>
 
<%-- 	<div>Welcome to bored main page,nice to have you here ${user.username}</div> --%> 
	
	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<form id="logoutForm" method="POST" action="/logout"></form>
	</c:if>
	
<div class="cards-container">

<c:forEach var="post" items="${posts}">
<div class="card" style="width: 18rem;">
  <img src="data:image/jpg;base64, ${post.encodedImage}"
  alt="image"	style="width: 100%">
  <div class="card-body">
    <h5 class="card-title">${post.title }</h5>
    <p class="card-text">${post.description }</p>
    <p class="card-text">${post.time }</p>
    <p id="userName"> ${post.user.username}</p>
   
  </div>
 <c:forEach items="${post.comments}" var="comment">
 <p>${comment.authorName}</p>
 <p>${comment.text}</p>
	</c:forEach>
 	
  <!-- forEach for comments -->
   <form:form method="Post" action="/commentPost" onsubmit="return validationComment(${post.id})">
   <input name="postId" type="hidden" value="${post.id}"/>
   <input name="userName" type="hidden" value="${user.username}"/>
   <input id="comment${post.id}" name="comment" type="text" placeholder="comment"/>
  <div id="commentError${post.id}" class="error_style"><spring:message code="create-comment.length.error" /></div>
  <button type="submit">submit</button>
  </form:form>
</div>
</c:forEach>

</div>

</body>
<script type="text/javascript" src="js/scripts.js"></script>
</html>

