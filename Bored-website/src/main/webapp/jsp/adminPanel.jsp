<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Accounts</title>
<title>Bored welcome page</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="main">Main</a>
      <a class="nav-item nav-link" href="createPost">Add Post</a>
      <a class="nav-item nav-link" href="myPosts">Show My Posts</a>
      <sec:authorize access="hasRole('ROLE_ADMIN')">
      <a class="nav-item nav-link" href="adminPanel">Admin Panel</a>
      </sec:authorize>
      <a class="nav-item nav-link" href="#" onclick="document.forms['logoutForm'].submit()">Exit</a>
    </div>
  </div>
</nav>

<c:if test="${pageContext.request.userPrincipal.name != null}">
		<form id="logoutForm" method="POST" action="/logout"></form>
	</c:if>

	<div>
		<table>
			<thead>
				<th>ID</th>
				<th>UserName</th>
				<th>Roles</th>
				<th>Actions</th>
			</thead>
			<c:forEach items="${allUsers}" var="user" >
			<tr>
				<td>${user.id}</td>
				<td>${user.username}</td>
				<td>${user.role}</td> 
				<td>
					<form action="/adminPanel" method="post">
						<input type="hidden" name="userId" value="${user.id}" /> 
						<input type="hidden" name="actionDelete" value="delete" />
						<button type="submit">Delete</button>
					</form>
				</td>
				<td>
				<form action="/changeUserRole" method="post">
						<input type="hidden" name="userId" value="${user.id}" /> 
						<input type="hidden" name="actionChangeRole" value="change" />
						<button type="submit">Change role</button>
				</form>
				</td> 
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>