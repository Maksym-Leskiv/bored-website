<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>No permission</title>
</head>
<body>
	<div>
		<div>
			<h3>You have no permission to access this page!</h3>
		</div>
		<form action="/logout" method="post">
			<input type="submit" value="Sign in as different user" /> <input
				type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		</form>
		<a href="main">return to main page</a>
	</div>
</body>
</html>