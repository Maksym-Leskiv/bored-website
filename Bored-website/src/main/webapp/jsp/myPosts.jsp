
<%@ page language="java" contentType="text/html; charset=utf-8"	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="css/myPosts.css">
<title>Your Posts</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="main">Main</a>
      <a class="nav-item nav-link" href="createPost">Add Post</a>
      <a class="nav-item nav-link" href="myPosts">Show My Posts</a>
      <sec:authorize access="hasRole('ROLE_ADMIN')">
      <a class="nav-item nav-link" href="adminPanel">Admin Panel</a>
      </sec:authorize>
      <a class="nav-item nav-link" href="#" onclick="document.forms['logoutForm'].submit()">Exit</a>
    </div>
  </div>
</nav>

<c:if test="${pageContext.request.userPrincipal.name != null}">
		<form id="logoutForm" method="POST" action="/logout"></form>
	</c:if>

<!-- <p id="infoText">here will be displayed your posts</p> -->

<div class="cards-container">
				<c:forEach var="post" items="${userPosts}">
				<div class="card" style="width: 18rem;">
					<img src="data:image/jpg;base64, ${post.encodedImage}" alt="image"	style="width: 100%">
					<div class="card-body">		
					<h5 class="card-title">${post.title }</h5>
					<p class="card-text">${post.description }</p>
	 				<p class="card-category">${post.category}</p>
					<p class="card-time">${post.time }</p>
				</div>
					
					<form action="${pageContext.request.contextPath}/myPosts"
						method="post">
						<input type="hidden" name="postId" value="${post.id}" /> 
						<input type="hidden" name="actionDelete" value="delete" />
						<button type="submit">Delete</button>
					</form>
					<br>
				<form:form action="${pageContext.request.contextPath}/myPostsUpdate"
						method="post" onsubmit="return validationPostUpdate(${post.id})">
						<input type="hidden" id="postId" name="postId" value="${post.id}" /> 
						<input type="text" id="postTitleUpdate${post.id}" name="postTitleUpdate" placeholder="update your post title">
						<div id="postTitleUpdateError${post.id}" class="error_style"><spring:message code="myPost.title.error" /></div>
						
						<input type="text" id="postDescriptionUpdate${post.id}" name="postDescriptionUpdate" placeholder="update your post description">
						<div id="postDescriptionUpdateError${post.id}" class="error_style"><spring:message code="myPost.description.error" /></div>
						<br><br>
						<button type="submit">update</button>
				</form:form>
				
				</div>
				</c:forEach>
</div>
</body>
</html>