<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Bored log in</title>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<link rel="stylesheet" type="text/css" href="css/login.css">
<link rel="preconnect" href="https://fonts.googleapis.com">

<link href="https://fonts.googleapis.com/css2?family=Permanent+Marker&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Gochi+Hand&family=Permanent+Marker&display=swap" rel="stylesheet">
</head>
<body>
<div class="container">

	<!-- Main Form -->
	<div class="login-form"> <!-- user -->
	
	<h1>Welcome</h1>
		<form method="POST" action="/login" id="login-form">
			
					<div class="form-group">
					    <input name="username" type="text"  class="form-field" 
					    placeholder="<spring:message code="login.username" /> "/>
					</div>
					
					<div class="form-group">
						
						<input name="password" type="password" class="form-field "
						 placeholder="<spring:message code="login.password" />"/>
					</div>
					
				 <button class="login-button"  type="submit"> <spring:message code="login.submit" /></button>
		
		</form>
		
		<div class="etc-login-form">
				<p class="Gochi_Hand"> <spring:message code="login.new_user" />
				 <a href="#" onclick="myFunction('showHide')"><spring:message code="login.create_account" />
				 </a></p>
				
			</div>
		
 <div id="showHide" style=display:none >
 <div class="container-two">

		<div class="login-form-1">

			<form:form  method="POST" action="/registration" modelAttribute="userForm" onsubmit="return validateForm()">
				<h2 class="form-signin-heading logo"><spring:message code="login.registration" /></h2>

				<div class="login-group">

						<div class="form-group">
								<input type="text" name="username" id="name" class="form-field" 
								 placeholder="<spring:message code="login.register_form_name" />"/>
							</div>
								
						<div id="name_error" class="error_style"><spring:message code="login.name.error" /></div>
							
					
					<div class="form-group">
								<input  type="email" name="email" id="email" class="form-field"
									placeholder="<spring:message code="login.register_form_email" />" />
							</div>

					<div id="email_error" class="error_style"><spring:message code="login.email.error" /></div>
					
					<div class="form-group">
								<input  type="password" name="password" id="password" 
								 class="form-field" minlength="8" maxlength="30"
								 placeholder="<spring:message code="login.register_form_password" />" />
							</div>
							
					<div id="password_error" class="error_style"><spring:message code="login.password.error" /></div>

					
					<div class="form-group">
								<input type='password' name="passwordConfirm" id="password_confirm" 
								class="form-field"
								 placeholder="<spring:message code="login.register_form_password_confirm" />" />
							</div>
					
					<div id="passwordC_err" class="error_style"><spring:message code="login.passwordC.error" /> </div>
					
				</div>
				<button class="login-button"  type="submit"> <spring:message code="login.submit" /></button>
				 
			</form:form>
		</div>

		<div class="etc-login-form">
			<p class="Gochi_Hand">
				<spring:message code="login.register_have_account" /> <a href="login" ><spring:message code="login.sign_in" /></a>
			</p>
		</div>



	</div>
</div>
</div>
<div class="language-div">
		<fieldset class="fieldset">
			<label class="Gochi_Hand"><spring:message code="login.choose_language" /></label>
			
			<select id="locales" class="form-control">
		    
		     	<option value="en"> <spring:message code='login.english' /> </option>
				<option value="ua"> <spring:message code='login.ukrainian' /> </option> 

			</select>
		</fieldset>
	</div>
</div>
</body>
</html>