
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Create Post</title>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/createPost.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
     <a class="nav-item nav-link" href="main">Main</a>
      <a class="nav-item nav-link" href="createPost">Add Post</a>
      <a class="nav-item nav-link" href="myPosts">Show My Posts</a>
      <sec:authorize access="hasRole('ROLE_ADMIN')">
      <a class="nav-item nav-link" href="adminPanel">Admin Panel</a>
      </sec:authorize>
      <a class="nav-item nav-link" href="#" onclick="document.forms['logoutForm'].submit()">Exit</a>
    </div>
  </div>
</nav>

<c:if test="${pageContext.request.userPrincipal.name != null}">
		<form id="logoutForm" method="POST" action="/logout"></form>
	</c:if>

	<div class="container">

		<form:form method="POST" action="/createPost"  onsubmit="return validationPost()"
			enctype="multipart/form-data" >
			<table>

				<tr>
					<td><spring:message code='create-post-title' /></td>
					<td><input id="title" type="text" name="title" /></td>
					<td><div id="titleError" class="error_style"><spring:message code="create-post.title.error" /></div></td> 
				</tr>
					
				<tr>
					<td><spring:message code='create-post-description' /></td>
					<td><input id="description" type="text" name="description" /></td>
					<td><div id="descriptionError" class="error_style"><spring:message code="create-post.description.error" /></div></td> 
				</tr>

				<tr>
					<td><spring:message code='create-post-category' /></td>
					
					
					<td>
					<label for="category"></label>
					<input name="category" list="category_post"/> 
					
  					<datalist id="category_post">
    				<option value="activity">activity</option>
    				<option value="culture">culture</option>
    				<option value="board games">board games</option>
    				<option value="other">other</option>
  					</datalist>
 				 </td>	
				</tr>
				<tr>
					<td><spring:message code='create-post-time' /></td>
					<td><input  type="datetime-local" id="meeting-time" name="meetingTime" min=""></td>
					<td><div id="timeError" class="error_style"><spring:message code="create-post.meetingTime.error" /></div></td>
				</tr>

				<tr>
					<td><spring:message code='create-post-select-image' /></td>
					<td><input type="file" name="image" id="image"
						accept=".png, .jpg, .jpeg" /></td>
				</tr>

<tr>
  <td style="display:none"><input type="text" name= "userId" value="${currentUser.id}"/></td>
</tr>

				<tr>
					<td><input type="submit" <spring:message code='create-post-submit' />/>
					</td>
				</tr>
					
			
			</table>
		</form:form>

	</div>
	
</body>
<script type="text/javascript" src="js/scripts.js"></script>
</html>