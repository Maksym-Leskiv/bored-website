
$(document).ready(function() {
	var selItem = localStorage.getItem("locales");
	$('#locales').val(selItem ? selItem : 'en');
	$("#locales").change(function() {
		var selectedOption = $('#locales').val();
		if (selectedOption) {
			window.location.replace('?lang=' + selectedOption);
			localStorage.setItem("locales", selectedOption);
		}
	});
});

function myFunction(id) {
	var x = document.getElementById(id);
	if (x.style.display === 'none') {
		x.style.display = 'block';
	} else {
		x.style.display = 'none';
	}
};

function validateForm(){

let password = document.getElementById('password').value;

let passwordC = document.getElementById('password_confirm').value;

let userName = document.getElementById('name').value;

let userEmail = document.getElementById('email').value;
	
	let nameError = true;
	   
	let emailError = true;
	
	let passError = true;
	
	let confirmError = true;
	
	let letters = /^[A-Za-z]+$/;
	
	let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	
	let passwordformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}$/;

	if(letters.test(userName) === false){
	nameError = false;
	document.getElementById('name_error').style.display = "initial";
	}else{
		document.getElementById('name_error').style.display = "none";
	}

	if(mailformat.test(userEmail) === false){
	emailError = false;
	document.getElementById('email_error').style.display = "initial";
	}else{
		document.getElementById('email_error').style.display = "none";
	}

	if(passwordformat.test(password) === false){
	passError = false;
	document.getElementById('password_error').style.display = "initial";
	}else{
		document.getElementById('password_error').style.display = "none";
	}

	if(password != passwordC){
	confirmError = false;
	document.getElementById('passwordC_err').style.display = "initial";
	}else{
		document.getElementById('passwordC_err').style.display = "none";
	}

	if((nameError && emailError && passError && confirmError) != true){
	return false;
	}

}

function validationPost(){
	
    let time = document.getElementById('meeting-time').value;
	
	let description = document.getElementById('description').value;
	
	let title = document.getElementById('title').value;
	
	let timeError = true;
	
	let descriptionError = true;
	
	let titleError = true;
	
	let today = Date.now();

	if(title.length < 2){
		titleError = false;
		document.getElementById('titleError').style.display = "initial";
	}else{
		document.getElementById('titleError').style.display = "none";
	}
	
	let meetingTime = Date.parse(time);
	
	if(meetingTime < today){
		timeError = false;
	document.getElementById('timeError').style.display = "initial";
	}else{
		document.getElementById('timeError').style.display = "none";
	}
	
	if(description.length < 20){
		descriptionError = false;
		document.getElementById('descriptionError').style.display = "initial";
	}else{
		document.getElementById('descriptionError').style.display = "none";
	}
			
	if((titleError && descriptionError && timeError) != true){
		return false;
	}
	
}

function validationPostUpdate(postID){

	let postTitleUpdate = document.getElementById('postTitleUpdate'+postID).value;
	
	let postDescriptionUpdate = document.getElementById('postDescriptionUpdate'+postID).value;

	let postTitleUpdateError = true;
	
	let postDescriptionUpdateError = true;
	
	if(postTitleUpdate.length > 0 && postTitleUpdate.length <= 2 ){	
		postTitleUpdateError = false;
		document.getElementById('postTitleUpdateError'+postID).style.display = "initial";
			
	} else{
		document.getElementById('postTitleUpdateError'+postID).style.display = "none";
	}
	
	if(postDescriptionUpdate.length > 0 && postDescriptionUpdate.length <= 20){
		postDescriptionUpdateError = false;
		document.getElementById('postDescriptionUpdateError'+postID).style.display = "initial";
	}else{
		document.getElementById('postDescriptionUpdateError'+postID).style.display = "none";
	}
	
	if(( postTitleUpdateError&& postDescriptionUpdateError) != true){
		return false;
	}
}

function validationComment(postID){
	
	let comment = document.getElementById('comment'+postID).value;
	
	let commentLengthError = true;
	
	if(comment.length > 250){
		commentLengthError = false;
		document.getElementById('commentError'+postID).style.display = "initial";
	}else{
		document.getElementById('commentError'+postID).style.display = "none";
	}
	
	if(commentLengthError != true){
		return false;
	}
}

let uploadFiel = document.getElementById('image'); 

uploadFiel.onchange = function() {						
    if(this.files[0].size > 10485760){
       uploadFielError = false;
       alert("File is too big!");
       this.value = "";
    };
	};

