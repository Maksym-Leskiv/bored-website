package com.dataart.internship.constants;

public class DefaultAppConstants {

	/* Group of constants for page URL */
	public static final String LOGIN_PAGE_URL = "/login";
	public static final String MAIN_PAGE_URL = "/main";
	public static final String ADMIN_PANEL_PAGE_URL = "/adminPanel";	
	public static final String MY_POSTS_PAGE_URL = "/myPosts/{username}";
	public static final String MY_POSTS_UPDATE_PAGE_URL = "updateTitleDescription/{id}";
	public static final String ACCESS_DENIED_PAGE_URL = "/403";
	public static final String CREATE_POST_PAGE_URL = "/createPost";
	public static final String REGISTRATION_PAGE_URL = "/registration";
	public static final String DELETE_POST_URL = "deletePost/{id}";
	public static final String COMMENT_POST_PAGE_URL = "/commentPost/{id}";
	public static final String WELCOME_PAGE_URL = "/welcomePage";
	public static final String INDEX_PAGE_URL = "/";
	public static final String DELETE_USER_URL = "deleteUser/{id}";
	
	/*Group of constants for Model Attribute use*/
	public static final String MODEL_ATTRIBUTE_CURRENT_USER = "currentUser";
	public static final String MODEL_ATTRIBUTE_PASSWORD_ERROR = "PasswordError";
	public static final String MODEL_ATTRIBUTE_POSTS = "posts";
	public static final String MODEL_ATTRIBUTE_USER = "user";
	public static final String MODEL_ATTRIBUTE_USER_EXSISTS = "UserExsists";
	public static final String MODEL_ATTRIBUTE_USER_FORM = "userForm";
	public static final String MODEL_ATTRIBUTE_USER_POSTS = "userPosts";
	public static final String MODEL_ATTRIBUTE_LIST_OF_USERS = "allUsers";
		
	/*Group of constants for Security*/
	public static final String SECRET = "javaRocks";
	public static final long EXPIRATION_TIME = 86_400_000; // 1 day
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/signup";

	/* Group of constants for Resources */
	public static final String CSS_RESOURCE_DIRECTORY = "/css/**";
	public static final String ADMIN_PANEL_RESOURCE = "/adminPanel/**";
	public static final String IMAGE_RESOURCE_DIRECTORY = "/images/**";
	public static final String JS_RESOURCE_DIRECTORY = "/js/**";
	
	/*Group of constants for User Roles*/
	public static final String USER_HAS_ANY_ROLE = "hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')";
	public static final String USER_HAS_ROLE_ADMIN = "hasRole('ROLE_ADMIN')";
	public static final String USER_ROLE_UPDATE_URL = "/changeUserRole/{id}";
	
	/* Group of constants for Actions */
	public static final String CHANGE_ACTION = "change";
	public static final String DELETE_ACTION = "delete";
	public static final String REDIRECT_ACTION = "redirect:";

	/* Group of constants for creating default administrator user in DB, when starting for the first time */
	public static final String DEFAULT_USER_NAME_ADMIN = "admin";
	public static final String DEFAULT_USER_PASSWORD_FOR_ADMIN = "1";
	public static final Long DEFAULT_USER_ADMIN_ID = 1L;
	public static final String DEFAULT_USER_ADMIN_EMAIL = "admin@admin.com";
	
	/* Group of constants for internationalization*/
	public static final String LOCAL_CHANGE_INTERCEPTOR_PARAM_NAME = "lang";
	public static final String MESSAGE_SOURCE_BASE_NAME = "classpath:i18n/message";
	public static final String MESSAGE_SOURCE_DEFAULT_ENCODING = "UTF-8";
	
	/*Group of constants for images*/
	public static final String IMAGE_EXTENSION = "image/jpeg";
	public static final String IMAGE_POST = "Post.jpg";
	public static final String SOURCE_DEFAULT_IMAGE = "src/main/webapp/images/Post.jpg";
	
	public static final String VIEW_RESOLVER_PREFIX = "/jsp/";
	public static final String VIEW_RESOLVER_SUFFIX = ".jsp";
	
	public static final String CONSTANT_CLASS = "Constant class";

	public static final String DATE_TIME_PATTERN = "dd-MM-yyyy HH:mm";
	
	private DefaultAppConstants() {
		throw new IllegalStateException(CONSTANT_CLASS);
	}
}
