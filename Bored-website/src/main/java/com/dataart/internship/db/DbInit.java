package com.dataart.internship.db;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.dataart.internship.entity.Role;
import com.dataart.internship.entity.User;
import com.dataart.internship.repository.UserRepository;

@Component
public class DbInit {

	@Autowired 
	private UserRepository userRepository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@PostConstruct
	public void postConstruct() {
		Optional<User> user = userRepository.findById((long) 1);
		if(user.empty().toString() == "Optional.empty") {
			User admin = new User();
			admin.setId((long)1);
			admin.setUsername("admin");
			admin.setRole(Role.ROLE_ADMIN);
			admin.setPassword(bCryptPasswordEncoder.encode("1"));
			
			userRepository.save(admin);
		}
	}
}
