package com.dataart.internship.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Size(max = 30)
	private String title;

	@Size(max = 250)
	private String description;
	private String category;
	private String time;

	@Lob
	private String encodedImage;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Cascade(CascadeType.DELETE)
	@OneToMany(mappedBy = "post", fetch = FetchType.EAGER)
	private List<Comment> comments;
}
