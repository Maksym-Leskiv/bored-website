package com.dataart.internship.entity;

public enum Role {
	ROLE_ADMIN, ROLE_USER
}
