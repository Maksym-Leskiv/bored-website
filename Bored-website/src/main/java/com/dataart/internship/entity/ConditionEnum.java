package com.dataart.internship.entity;

public enum ConditionEnum {

	ROLE_ADMIN {
		@Override
		public User apply(User user) {
			user.setRole(Role.ROLE_USER);
			return user;
		}
	},
	ROLE_USER {
		@Override
		public User apply(User user) {
			user.setRole(Role.ROLE_ADMIN);
			return user;
		}
	};

	public abstract User apply(User user);
}
