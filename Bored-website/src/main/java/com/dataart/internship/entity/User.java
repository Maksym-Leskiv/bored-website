package com.dataart.internship.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "user")
@Data
@NoArgsConstructor
@ToString
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true)
	private String email;

	@Column(unique = true)
	@Size(min = 2, message = "at least 2 characters")
	private String username;

	@Size(min = 8, message = "at least 8 characters")
	private String password;

	@Enumerated(EnumType.STRING)
	private Role role;
	
	@OneToMany(mappedBy = "user")
	@ToString.Exclude private Set<Post> posts;
	
	@Transient
	private String passwordConfirm;

	public User(@Size(min = 2, message = "at least 2 characters") String username, Role role) {
		super();
		this.username = username;
		this.role = role;
	}

	public User(User user) {
		this.id = user.getId();
		this.email = user.getEmail();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.role = user.getRole();
		this.posts = user.getPosts();
	}

	public User(@Size(min = 2, message = "at least 2 characters") String username,
			@Size(min = 8, message = "at least 8 characters") String password) {
		super();
		this.username = username;
		this.password = password;
	}
}
