package com.dataart.internship.mapper;

import java.util.Objects;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.dataart.internship.dto.PostDTO;
import com.dataart.internship.entity.Post;

@Component
public class PostMapper {

	@Autowired
	private ModelMapper mapper;
	
	public PostDTO toDTO(Post post) {
		return Objects.isNull(post) ? null : mapper.map(post, PostDTO.class);
	}
}
