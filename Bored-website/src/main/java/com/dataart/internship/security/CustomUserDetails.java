package com.dataart.internship.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.dataart.internship.entity.User;

public class CustomUserDetails extends User implements UserDetails {

	private String userRoles;

	public CustomUserDetails(User user, String role) {
		super(user);
		this.userRoles = role;
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		String roles = userRoles;
		return AuthorityUtils.commaSeparatedStringToAuthorityList(roles);
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
