package com.dataart.internship.controller;

import static com.dataart.internship.constants.DefaultAppConstants.CREATE_POST_PAGE_URL;
import static com.dataart.internship.constants.DefaultAppConstants.DELETE_POST_URL;
import static com.dataart.internship.constants.DefaultAppConstants.MAIN_PAGE_URL;
import static com.dataart.internship.constants.DefaultAppConstants.MY_POSTS_PAGE_URL;
import static com.dataart.internship.constants.DefaultAppConstants.MY_POSTS_UPDATE_PAGE_URL;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataart.internship.dto.PostDTO;
import com.dataart.internship.entity.Post;
import com.dataart.internship.entity.User;
import com.dataart.internship.mapper.PostMapper;
import com.dataart.internship.service.PostService;
import com.dataart.internship.service.UserService;

@RestController
public class PostController {

	private static final Logger logger = Logger.getLogger(PostController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private PostService postService;
	
	@Autowired
	private PostMapper postMapper;
	
	@PostMapping(value = CREATE_POST_PAGE_URL)
	public PostDTO addPost(@RequestBody PostDTO post) throws IOException {
		User user = userService.findByUsername(post.getUsername());
		String image = post.getEncodedImage();
		if (image.isEmpty()) {
			logger.info(String.format("user: %s created a post with a defaut photo", post.getUsername()));
			PostDTO savedPost = convertPostToPostDTO(postService.savePost(PostDTO.createEntity(post.getTitle(), post.getDescription(),
					post.getCategory(), post.getTime(),postService.createDefaultImage(), user)));
			return savedPost;
		} else {
			logger.info(String.format("user: %s created a post with his own photo", post.getUsername()));
			PostDTO savedPost = convertPostToPostDTO(
					postService.savePost(PostDTO.createEntity(post.getTitle(), post.getDescription(),
							post.getCategory(), post.getTime(), image, user)));
			return savedPost;
		}
	}
	
	@GetMapping(value = MAIN_PAGE_URL)
	public List<PostDTO> getUserPosts() {
		List<Post> allPosts = postService.getAllPosts();
		List<PostDTO> convertPostListToPostDTO = convertPostListToPostDTO(allPosts);
		logger.info(String.format("returned all posts"));
		return convertPostListToPostDTO;
	}

	@GetMapping(MY_POSTS_PAGE_URL)
	public List<PostDTO> userPosts(@PathVariable String username) {
		User user = userService.findByUsername(username);
		List<Post> posts = postService.getAllByUserId(user.getId());
		List<PostDTO> convertPostListToPostDTO = convertPostListToPostDTO(posts);
		logger.info(String.format("returned all posts, for user: %s", username));
		return convertPostListToPostDTO;
	}
	
	@DeleteMapping(DELETE_POST_URL)
	public void deletePost(@PathVariable Long id) {
		postService.deleteById(id);
		logger.info(String.format("post with id: %d was deleted", id));
	}

	@PutMapping(MY_POSTS_UPDATE_PAGE_URL)
	public PostDTO updatePost(@PathVariable Long id, @RequestBody PostDTO post) {
		Post postFromDB = postService.findPostById(Long.valueOf(id));
		if ( !post.getTitle().isEmpty()) {
			postFromDB.setTitle(post.getTitle());
			logger.info(String.format("post with id: %d updated title", id));
		}

		if (!post.getDescription().isEmpty()) {
			postFromDB.setDescription(post.getDescription());
			logger.info(String.format("post with id: %d updated description", id));
		}
		PostDTO postDTO = convertPostToPostDTO(postService.savePost(postFromDB));
		return postDTO;
	}
	
	private PostDTO convertPostToPostDTO(Post post) {
		PostDTO postDTO = new PostDTO();
		postDTO = postMapper.toDTO(post);
		return postDTO;
	}

	private List<PostDTO> convertPostListToPostDTO(List<Post> posts) {
		List<PostDTO> postDTOs = new ArrayList<>();
		posts.forEach(post -> postDTOs.add(convertPostToPostDTO(post)));
		return postDTOs;
	}
}
