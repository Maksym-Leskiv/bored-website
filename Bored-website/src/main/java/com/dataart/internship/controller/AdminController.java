package com.dataart.internship.controller;

import static com.dataart.internship.constants.DefaultAppConstants.ADMIN_PANEL_PAGE_URL;
import static com.dataart.internship.constants.DefaultAppConstants.DEFAULT_USER_ADMIN_ID;
import static com.dataart.internship.constants.DefaultAppConstants.DELETE_USER_URL;
import static com.dataart.internship.constants.DefaultAppConstants.USER_ROLE_UPDATE_URL;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dataart.internship.dto.UserDTO;
import com.dataart.internship.entity.User;
import com.dataart.internship.exceptions.AdminAccountException;
import com.dataart.internship.service.UserService;

@RestController
@RequestMapping(ADMIN_PANEL_PAGE_URL)
public class AdminController {

	private static final Logger logger = Logger.getLogger(AdminController.class);

	@Autowired
	private UserService userService;

	@GetMapping
	public List<UserDTO> userList() {
		return convertListUsersToUserDTO(userService.getAllUsers());
	}

	@GetMapping("{username}")
	public UserDTO getUserByUsername(@PathVariable String username) {
		return convertUserToUserDTO(userService.findByUsername(username));
	}

	@PutMapping(USER_ROLE_UPDATE_URL)
	public void changeUserRole(@PathVariable Long id) {
		userService.saveUserUpdatedRole(id);
		logger.info(String.format(" changed role for user with id: %d \n", id));
	}

	@DeleteMapping(DELETE_USER_URL)
	public void deleteEmployee(@PathVariable Long id) {
		if (id == DEFAULT_USER_ADMIN_ID) {
			throw new AdminAccountException("you can not delete a default admin account");
		} else {
			userService.deleteUser(id);
			logger.info(String.format(" deleted user with id: %d \n", id));
		}
	}

	private UserDTO convertUserToUserDTO(User user) {
		UserDTO userDTO = new UserDTO();
		BeanUtils.copyProperties(user, userDTO);
		return userDTO;
	}

	private List<UserDTO> convertListUsersToUserDTO(List<User> usersList) {
		List<UserDTO> users = new ArrayList<>();
		usersList.forEach(user -> users.add(convertUserToUserDTO(user)));
		return users;
	}
}
