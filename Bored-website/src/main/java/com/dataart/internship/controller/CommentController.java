package com.dataart.internship.controller;

import static com.dataart.internship.constants.DefaultAppConstants.COMMENT_POST_PAGE_URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataart.internship.dto.CommentDTO;
import com.dataart.internship.service.CommentService;

@RestController
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@PostMapping(COMMENT_POST_PAGE_URL)
	public void writeComment(@RequestBody CommentDTO comment,@PathVariable String id) {
		commentService.addComment(Long.valueOf(id), comment.getText(), comment.getAuthorName());
	}
}
