package com.dataart.internship.controller;

import static com.dataart.internship.constants.DefaultAppConstants.REGISTRATION_PAGE_URL;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dataart.internship.dto.UserDTO;
import com.dataart.internship.exceptions.PasswordDontMatchExeception;
import com.dataart.internship.exceptions.UserExistsExeption;
import com.dataart.internship.service.UserService;

@RestController
public class RegistrationController {

	private static final Logger logger = Logger.getLogger(RegistrationController.class);

	@Autowired
	private UserService userService;

	@PostMapping(REGISTRATION_PAGE_URL)
	public void addUser(@RequestBody UserDTO userForm) throws PasswordDontMatchExeception, UserExistsExeption{
		
		if (!userForm.getPassword().equals(userForm.getPasswordConfirm())) {
			throw new PasswordDontMatchExeception("passwords don't match");
		}

		if (!userService.saveUser(UserDTO.createEntity(userForm))) {
			logger.info(String.format("user: %s trying to register again", userForm.getUsername()));
		    throw new UserExistsExeption("User with this credential is already registred");
		}
		
		
		logger.info(String.format("registered a user %s", userForm.getUsername()));
	}
}
