package com.dataart.internship.service;

import com.dataart.internship.entity.Comment;

public interface CommentService {
	
	 void addComment(Long id, String text, String userName);
	 
	 Comment getCommentById(Long id);
	 
}
