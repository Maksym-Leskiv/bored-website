package com.dataart.internship.service.impl;

import static com.dataart.internship.constants.DefaultAppConstants.DEFAULT_USER_ADMIN_EMAIL;

import static com.dataart.internship.constants.DefaultAppConstants.DEFAULT_USER_ADMIN_ID;
import static com.dataart.internship.constants.DefaultAppConstants.DEFAULT_USER_NAME_ADMIN;
import static com.dataart.internship.constants.DefaultAppConstants.DEFAULT_USER_PASSWORD_FOR_ADMIN;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dataart.internship.entity.ConditionEnum;
import com.dataart.internship.entity.Role;
import com.dataart.internship.entity.User;
import com.dataart.internship.repository.UserRepository;
import com.dataart.internship.service.UserService;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

	private static final Logger logger = Logger.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public User findUserById(Long id) {
		if (userRepository.findById(id).isPresent()) {
			return userRepository.findById(id).get();
		} else {
			throw new UsernameNotFoundException(String.format("user with id: %d not found", id));
		}
	}

	@Override
	public boolean saveUser(User user) {
		User userFromDB = userRepository.findByUsername(user.getUsername());
		if (userFromDB != null) {
			logger.info(String.format("%s is not unique", userFromDB.getUsername()));
			return false;
		}
		user.setRole(Role.ROLE_USER);
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		userRepository.save(user);
		logger.info(String.format("%s is saved", user.getUsername()));
		return true;
	}

	@Override
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}

	@Override
	public void deleteUser(Long id) {
		userRepository.deleteById(id);
		logger.info(String.format("user with %d is deleted", id));
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user != null) {
			return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRole().name()));
		}
		throw new UsernameNotFoundException(String.format("User with UserName: %s doesnt exists", username));
	}

	@PostConstruct
	public void postConstruct() {
		Optional<User> user = userRepository.findById(DEFAULT_USER_ADMIN_ID);
		if (!user.isPresent()) {
			User admin = new User();
			admin.setId(DEFAULT_USER_ADMIN_ID);
			admin.setUsername(DEFAULT_USER_NAME_ADMIN);
			admin.setRole(Role.ROLE_ADMIN);
			admin.setPassword(bCryptPasswordEncoder.encode(DEFAULT_USER_PASSWORD_FOR_ADMIN));
			admin.setEmail(DEFAULT_USER_ADMIN_EMAIL);
			userRepository.save(admin);
		}
	}

	@Override
	public void saveUserUpdatedRole(Long id) {
		User user = userRepository.findById(id).get(); 
		User apply = ConditionEnum.valueOf(user.getRole().toString()).apply(user);
		userRepository.save(apply);
		logger.info(String.format("updated role for %s", user.getUsername()));
	}
}