package com.dataart.internship.service;

import java.io.IOException;
import java.util.List;

import com.dataart.internship.entity.Post;

public interface PostService {

	Post savePost(Post post);
	
	String createDefaultImage() throws IOException;
	
	List<Post> getAllPosts();
	
	Post findPostById(Long id); 
	
	void deleteById(Long id);
	
	List<Post> getAllByUserId(long id);
	
}
