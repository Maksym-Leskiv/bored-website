package com.dataart.internship.service;

import java.util.List;

import com.dataart.internship.entity.User;

public interface UserService {

	User findUserById(Long id);

	boolean saveUser(User user);
	
	void deleteUser(Long id);
	
	List<User> getAllUsers();
	
	User findByUsername(String username);

	void saveUserUpdatedRole(Long id);
	
}

