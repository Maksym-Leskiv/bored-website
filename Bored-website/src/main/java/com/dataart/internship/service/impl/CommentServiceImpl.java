package com.dataart.internship.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dataart.internship.entity.Comment;
import com.dataart.internship.entity.Post;
import com.dataart.internship.repository.CommentRepository;
import com.dataart.internship.service.CommentService;
import com.dataart.internship.service.PostService;

@Service
public class CommentServiceImpl implements CommentService {

	private static final Logger logger = Logger.getLogger(CommentServiceImpl.class);

	@Autowired
	private CommentRepository commentRepository;

	@Autowired
	private PostService postService;

	@Override
	public void addComment(Long id, String text, String userName) {
		Post postFromDB = postService.findPostById(id);
		Comment comment = new Comment();
		comment.setText(text);
		comment.setAuthorName(userName);
		comment.setPost(postFromDB);
		commentRepository.save(comment);
		logger.info(String.format("user with username: %s add comment to post with: %d", userName, id));
	}

	@Override
	public Comment getCommentById(Long id) {
		return commentRepository.findById(id).get();
	}
}
