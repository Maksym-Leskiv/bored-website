package com.dataart.internship.service.impl;

import static com.dataart.internship.constants.DefaultAppConstants.IMAGE_EXTENSION;
import static com.dataart.internship.constants.DefaultAppConstants.IMAGE_POST;
import static com.dataart.internship.constants.DefaultAppConstants.SOURCE_DEFAULT_IMAGE;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;

import com.dataart.internship.entity.Post;
import com.dataart.internship.exceptions.PostNotFoundException;
import com.dataart.internship.repository.PostRepository;
import com.dataart.internship.service.PostService;

@Service
public class PostServiceImpl implements PostService {

	private static final Logger logger = Logger.getLogger(PostServiceImpl.class);

	@Autowired
	private PostRepository postRepository;

	@Override
	public Post savePost(Post post) {
		return postRepository.save(post);
	}

	@Override
	public String createDefaultImage() throws IOException {
		File uploadFile = new File(SOURCE_DEFAULT_IMAGE);
		FileInputStream inputStream = new FileInputStream(uploadFile);
		MockMultipartFile image = new MockMultipartFile(IMAGE_POST, IMAGE_POST, IMAGE_EXTENSION,
				IOUtils.toByteArray(inputStream));
		String encodeToString = Base64.getEncoder().encodeToString(image.getBytes());
		return encodeToString;
	}

	@Override
	public List<Post> getAllPosts() {
		return postRepository.findAll();
	}

	@Override
	public Post findPostById(Long id) {
		if (postRepository.findById(id).isPresent()) {
			return postRepository.findById(id).get();
		} else {
			logger.info(String.format("Post with id: %d not found", id));
			throw new PostNotFoundException(String.format("Post with id: %d not found", id));
		}
	}

	@Override
	public void deleteById(Long id) {
		postRepository.deleteById(id);
		logger.info(String.format("post with id: %d was deleted", id));
	}

	@Override
	public List<Post> getAllByUserId(long id) {
		return postRepository.getAllByUserId(id);
	}
}
