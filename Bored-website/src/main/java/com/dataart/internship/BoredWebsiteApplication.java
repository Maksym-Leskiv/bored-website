package com.dataart.internship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoredWebsiteApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoredWebsiteApplication.class, args);
	}
}
