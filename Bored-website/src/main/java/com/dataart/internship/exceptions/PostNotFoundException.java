package com.dataart.internship.exceptions;

public class PostNotFoundException extends RuntimeException{

	public PostNotFoundException(String msg) {
		super(msg);
	}
}
