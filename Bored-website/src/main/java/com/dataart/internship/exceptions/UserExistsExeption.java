package com.dataart.internship.exceptions;

public class UserExistsExeption extends RuntimeException{

	public UserExistsExeption(String msg) {
		super(msg);
	}
}
