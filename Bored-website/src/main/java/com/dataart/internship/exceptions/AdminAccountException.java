package com.dataart.internship.exceptions;

public class AdminAccountException extends RuntimeException{

	public AdminAccountException(String msg) {
		super(msg);
	}
}
