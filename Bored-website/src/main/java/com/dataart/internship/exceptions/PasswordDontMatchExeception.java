package com.dataart.internship.exceptions;

public class PasswordDontMatchExeception extends RuntimeException{

	public PasswordDontMatchExeception(String msg) {
		super(msg);
	}
}
