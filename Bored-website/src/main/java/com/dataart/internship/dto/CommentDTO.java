package com.dataart.internship.dto;

import lombok.Data;

@Data
public class CommentDTO {
	private Long id;
	private String text;
	private String authorName;
}
