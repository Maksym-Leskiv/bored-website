package com.dataart.internship.dto;

import com.dataart.internship.entity.Role;
import com.dataart.internship.entity.User;

import lombok.Data;

@Data
public class UserDTO {
	
	private Long id;
	private String username;
	private String email;
	private String password;
	private Role role;
	private String passwordConfirm;
	
	public static User createEntity(UserDTO user) {
		User userAccount = new User();
		userAccount.setUsername(user.getUsername());
		userAccount.setEmail(user.getEmail());
		userAccount.setPassword(user.getPassword());
		userAccount.setRole(user.getRole());
		return userAccount;
	}
}
