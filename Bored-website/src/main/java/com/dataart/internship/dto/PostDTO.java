package com.dataart.internship.dto;

import static com.dataart.internship.constants.DefaultAppConstants.DATE_TIME_PATTERN;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.dataart.internship.entity.Post;
import com.dataart.internship.entity.User;

import lombok.Data;

@Data
public class PostDTO {

	private Long id;
	private String title;
	private String description;
	private String category;
	private String time;
	private String encodedImage;
	private String username;
	private List<CommentDTO> comments;
	
	public static Post createEntity(String title, String description, String category, String meetingTime,
		String encodedImage, User user) throws IOException {
		LocalDateTime parse = LocalDateTime.parse(meetingTime);
		DateTimeFormatter format = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
		Post post = new Post();
		post.setTitle(title);
		post.setDescription(description);
		post.setCategory(category);
		post.setTime(parse.format(format));
		post.setEncodedImage(encodedImage);
		post.setUser(user);
		return post;
	}
}
