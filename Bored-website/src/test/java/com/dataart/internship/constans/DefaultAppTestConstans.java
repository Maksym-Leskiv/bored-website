package com.dataart.internship.constans;

public class DefaultAppTestConstans {

	/* Group of constants parameters*/
	public static final String PARAM_ACTION_DELETE = "actionDelete";
	public static final String PARAM_ACTION_DELETE_VALUE = "delete";
	public static final String PARAM_CATEGORY = "category";
	public static final String PARAM_CATEGORY_VALUE = "TEST";
	public static final String PARAM_DESCRIPTION = "description";
	public static final String PARAM_DESCRIPTION_VALUE = "post from test case";
	public static final String PARAM_EMAIL = "email";
	public static final String PARAM_EMAIL_TEST = "testUser@mail.com";
	public static final String PARAM_MEETING_TIME = "meetingTime";
	public static final String PARAM_MEETING_TIME_VALUE = "2021-08-01T09:19";
	public static final String PARAM_NAME_TEST = "test";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_POST_DESCRIPTION_UPDATE = "postDescriptionUpdate";
	public static final String PARAM_POST_ID = "postId";
	public static final String PARAM_POST_TITLE_UPDATE = "postTitleUpdate";
	public static final String PARAM_POST_UPDATE_VALUE = "test text";
	public static final String PARAM_TITLE = "title";
	public static final String PARAM_TITLE_VALUE = "here is title";
	public static final String PARAM_USER_ID = "userId";
	public static final String PARAM_USER_NAME = "username";
	public static final String POST_PARAM_ACTION_DELETE = "actionDelete";
	public static final String POST_PARAM_COMMENT = "comment";
	public static final String POST_PARAM_COMMENT_TEXT = "test comment from test case";
	public static final String POST_PARAM_DELETE = "actionDelete";
	public static final String POST_PARAM_POST_ID = "postId";
	public static final String POST_PARAM_USER_ID = "userId";
	public static final String POST_PARAM_USER_NAME = "userName";
	
	/* Group of constants for URL */
 	public static final String ADMIN_PANEL_PAGE_URL = "/adminPanel";
	public static final String COMMENT_POST_PAGE_URL = "/commentPost";
	public static final String CREATE_POST_PAGE_URL = "/createPost";
	public static final String INDEX_PAGE_URL = "/";
	public static final String LOCAL_HOST_LOGIN_PAGE_URL = "http://localhost/login";
	public static final String LOGIN_ERROR_PAGE_URL = "/login?error";
	public static final String LOGIN_PAGE_URL = "/login";
	public static final String MAIN_PAGE_URL = "/main";
	public static final String MY_POSTS_PAGE_URL = "/myPosts";
	public static final String MY_POSTS_UPDATE_URL = "/myPostsUpdate";
	public static final String REGISTRATION_PAGE_URL = "/registration";
	
	/*Group of constants for Roles and authorities*/
	public static final String USER_ADMIN = "admin";
	public static final String USER_AUTHORITIES_ROLE_ADMIN = "ROLE_ADMIN";
	public static final String USER_AUTHORITIES_ROLE_USER = "ROLE_USER";
	public static final String AUTHENTICATED_ROLE_ADMIN = "ADMIN";
	public static final String USER_DETAILS_USER = "adminDB";	
	public static final String MOCK_USER_ROLE_ADMIN = "ADMIN";
	public static final String MOCK_USER_ROLE_USER = "USER";
	
	/*Group of constants for test objects*/
	public static final String TEST_USER = "testUser";
	public static final String USER_NAME_VALUE_ONE = "UserOne";
	public static final String USER_NAME_VALUE_TWO = "UserTwo";
	public static final String USER_TEST = "test";
	public static final String MOCK_USER_TESTER = "tester";
		
	/* Group of constants for testing PostController*/
	public static final String MOCK_MULTIPART_FILE_FORM_DATA = "multipart/form-data";
	public static final String MOCK_MULTIPART_FILE_NAME = "Post.jpg";
	public static final String MOCK_MULTIPART_FILE_TYPE = "image";
	
	public static final String CONSTANT_CLASS = "Constant test class";
	
	public static final String SOURCE_DEFAUL_IMAGE = "src/main/webapp/images/Post.jpg";
	
	public static final String DEFAULT_NUMBER_ONE = "1";
	
	private DefaultAppTestConstans() {
		throw new IllegalStateException(CONSTANT_CLASS);
	}
}
