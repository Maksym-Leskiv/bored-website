package com.dataart.internship.unitTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.ADMIN_PANEL_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_ADMIN;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_AUTHORITIES_ROLE_ADMIN;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_AUTHORITIES_ROLE_USER;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_NAME_VALUE_ONE;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_NAME_VALUE_TWO;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.dataart.internship.controller.AdminController;
import com.dataart.internship.entity.Role;
import com.dataart.internship.entity.User;
import com.dataart.internship.service.impl.UserServiceImpl;

@WebMvcTest(AdminController.class)
@RunWith(SpringRunner.class)
public class AdminControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserServiceImpl userServiceImpl;

	@Test
	@WithMockUser(username = USER_ADMIN, authorities = { USER_AUTHORITIES_ROLE_ADMIN,
			USER_AUTHORITIES_ROLE_USER})
	public void testUserList() throws Exception {

		List<User> allUsersList = new ArrayList<>();

		allUsersList.add(new User(USER_NAME_VALUE_ONE, Role.ROLE_USER));
		allUsersList.add(new User(USER_NAME_VALUE_TWO, Role.ROLE_USER));

		Mockito.when(userServiceImpl.getAllUsers()).thenReturn(allUsersList);
		Assert.assertNotNull(allUsersList);
		mockMvc.perform(get(ADMIN_PANEL_PAGE_URL)).andExpect(status().isOk());
	}
}
