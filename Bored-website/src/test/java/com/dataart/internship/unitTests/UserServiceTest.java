package com.dataart.internship.unitTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.DEFAULT_NUMBER_ONE;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_TEST;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.dataart.internship.entity.Role;
import com.dataart.internship.entity.User;
import com.dataart.internship.repository.UserRepository;
import com.dataart.internship.service.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

	@Autowired
	private UserService userService;
	
	@MockBean
	private UserRepository userRepository;
		
	@Test
	public void testSaveUser() {
		User user = new User();
		user.setUsername(USER_TEST);
		user.setPassword(DEFAULT_NUMBER_ONE);
		Assert.assertTrue(userService.saveUser(user));
		Assert.assertTrue(CoreMatchers.is(user.getRole()).matches(Role.ROLE_USER));
		Mockito.verify(userRepository, Mockito.times(1)).save(user);
		Mockito.verify(userRepository, Mockito.times(1)).findByUsername(user.getUsername());
	}
}
