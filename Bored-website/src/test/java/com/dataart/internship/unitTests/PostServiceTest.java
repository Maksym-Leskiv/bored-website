package com.dataart.internship.unitTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.SOURCE_DEFAUL_IMAGE;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.dataart.internship.entity.Post;
import com.dataart.internship.repository.PostRepository;
import com.dataart.internship.service.impl.PostServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PostServiceTest {

	@Autowired
	private PostServiceImpl postServiceImpl;

	@MockBean
	private PostRepository postRepository;

//	@Test
//	public void testSavePost() {
//		Post post = new Post();
//		Assert.assertTrue(postServiceImpl.savePost(post));
//		Mockito.verify(postRepository, Mockito.times(1)).save(post);
//	}

	@Test
	public void testCreateDefaultImage() throws FileNotFoundException {
		File uploadFile = new File(SOURCE_DEFAUL_IMAGE);
		FileInputStream inputStream = new FileInputStream(uploadFile);
		Assert.assertNotNull(inputStream);
	}
}
