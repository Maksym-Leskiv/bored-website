package com.dataart.internship.unitTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_COMMENT_TEXT;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_TEST;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.dataart.internship.service.CommentService;
import com.dataart.internship.service.PostService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CommentServiceTest {

	@Autowired
	private CommentService commentService;

	@MockBean
	private PostService postService;
	
	@Test
	public void testAddComment() throws Exception{
		commentService.addComment(1L, POST_PARAM_COMMENT_TEXT, USER_TEST);
		Mockito.verify(postService, Mockito.times(1)).findPostById(1L);
	}
}
