package com.dataart.internship.integrationTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.ADMIN_PANEL_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.DEFAULT_NUMBER_ONE;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_USER_ROLE_ADMIN;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_USER_ROLE_USER;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_USER_TESTER;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_ACTION_DELETE;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_DELETE;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_USER_ID;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class AdminControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@WithMockUser(username = MOCK_USER_TESTER, password = DEFAULT_NUMBER_ONE, roles = MOCK_USER_ROLE_ADMIN )
	void deleteUserTest() throws Exception{
		this.mockMvc.perform(post(ADMIN_PANEL_PAGE_URL)
				.param(POST_PARAM_USER_ID, DEFAULT_NUMBER_ONE)
				.param(POST_PARAM_ACTION_DELETE, POST_PARAM_DELETE))
		.andDo(print())
		.andExpect(status().is4xxClientError());
	}
	
	@Test
	@WithMockUser(username = MOCK_USER_TESTER, password = DEFAULT_NUMBER_ONE, roles = MOCK_USER_ROLE_USER)
	void deleteUserNoPermitionTest() throws Exception{
		this.mockMvc.perform(post(ADMIN_PANEL_PAGE_URL)
				.param(POST_PARAM_USER_ID, DEFAULT_NUMBER_ONE)
				.param(POST_PARAM_ACTION_DELETE, POST_PARAM_DELETE))
		.andDo(print())
		.andExpect(status().is4xxClientError());
	}
}
