package com.dataart.internship.integrationTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.AUTHENTICATED_ROLE_ADMIN;
import static com.dataart.internship.constans.DefaultAppTestConstans.DEFAULT_NUMBER_ONE;
import static com.dataart.internship.constans.DefaultAppTestConstans.INDEX_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.LOCAL_HOST_LOGIN_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.LOGIN_ERROR_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.LOGIN_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.MAIN_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_EMAIL;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_EMAIL_TEST;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_NAME_TEST;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_PASSWORD;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_USER_NAME;
import static com.dataart.internship.constans.DefaultAppTestConstans.REGISTRATION_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.TEST_USER;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_ADMIN;
import static org.junit.Assert.assertNotNull;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.dataart.internship.controller.RegistrationController;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LoginTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private RegistrationController registrationController;
	
	@Test
	@Order(1)
	void test(){
		assertNotNull(registrationController);		
	}
	
	@Test
	@Order(2)
	void testGetWelcomePage() throws Exception{
		this.mockMvc.perform(get(INDEX_PAGE_URL))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test
	@Order(3)
	void accessDenidedTest() throws Exception{
		this.mockMvc.perform(get(MAIN_PAGE_URL))
		.andDo(print())
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl(LOCAL_HOST_LOGIN_PAGE_URL));
	}
	
	@Test
	@Order(4)
	void badCredentialsLoginTest() throws Exception{
		this.mockMvc.perform(formLogin().user(TEST_USER).password(DEFAULT_NUMBER_ONE))
		.andDo(print())
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl(LOGIN_ERROR_PAGE_URL));
	}
	
	@Test
	@Order(5)
	void badCredentialsTest() throws Exception{
		this.mockMvc.perform(post(LOGIN_PAGE_URL).param(PARAM_USER_NAME,PARAM_NAME_TEST).param(PARAM_PASSWORD, PARAM_PASSWORD))
		.andDo(print())
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl(LOGIN_ERROR_PAGE_URL));
	}
	
	@Test
	@Order(6)
	void registerUserForTest() throws Exception{
		this.mockMvc.perform(post(REGISTRATION_PAGE_URL).param(PARAM_USER_NAME,TEST_USER)
				.param(PARAM_EMAIL, PARAM_EMAIL_TEST)
				.param(PARAM_PASSWORD, DEFAULT_NUMBER_ONE))
		.andDo(print())
		.andExpect(status().is2xxSuccessful());
	}
	
	@Test
	@Order(7)
	void accessSuccessfullyTest() throws Exception{
		this.mockMvc.perform(formLogin().user(USER_ADMIN).password(DEFAULT_NUMBER_ONE))
		.andDo(print())
		.andExpect(authenticated().withUsername(USER_ADMIN).withRoles(AUTHENTICATED_ROLE_ADMIN))
		.andExpect(status().is3xxRedirection())
		.andExpect(redirectedUrl(MAIN_PAGE_URL));	
	}
}