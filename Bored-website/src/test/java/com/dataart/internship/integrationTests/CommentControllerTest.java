package com.dataart.internship.integrationTests;
import static com.dataart.internship.constans.DefaultAppTestConstans.COMMENT_POST_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.DEFAULT_NUMBER_ONE;
import static com.dataart.internship.constans.DefaultAppTestConstans.MAIN_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_USER_TESTER;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_COMMENT;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_COMMENT_TEXT;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_POST_ID;
import static com.dataart.internship.constans.DefaultAppTestConstans.POST_PARAM_USER_NAME;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "tester", password = "1")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-post-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-comment-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Sql(value = {"/create-post-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Sql(value = {"/create-user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class CommentControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void addCommentTest() throws Exception {
		this.mockMvc.perform(post(COMMENT_POST_PAGE_URL)
				.param(POST_PARAM_POST_ID, DEFAULT_NUMBER_ONE)
				.param(POST_PARAM_COMMENT, POST_PARAM_COMMENT_TEXT)
				.param(POST_PARAM_USER_NAME, MOCK_USER_TESTER))
				.andDo(print())
				.andExpect(authenticated());
	}
}
