package com.dataart.internship.integrationTests;

import static com.dataart.internship.constans.DefaultAppTestConstans.CREATE_POST_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.DEFAULT_NUMBER_ONE;
import static com.dataart.internship.constans.DefaultAppTestConstans.MAIN_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_MULTIPART_FILE_FORM_DATA;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_MULTIPART_FILE_NAME;
import static com.dataart.internship.constans.DefaultAppTestConstans.MOCK_MULTIPART_FILE_TYPE;
import static com.dataart.internship.constans.DefaultAppTestConstans.MY_POSTS_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.MY_POSTS_UPDATE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_ACTION_DELETE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_ACTION_DELETE_VALUE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_CATEGORY;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_CATEGORY_VALUE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_DESCRIPTION;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_DESCRIPTION_VALUE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_MEETING_TIME;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_MEETING_TIME_VALUE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_POST_DESCRIPTION_UPDATE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_POST_ID;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_POST_TITLE_UPDATE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_POST_UPDATE_VALUE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_TITLE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_TITLE_VALUE;
import static com.dataart.internship.constans.DefaultAppTestConstans.SOURCE_DEFAUL_IMAGE;
import static com.dataart.internship.constans.DefaultAppTestConstans.PARAM_USER_ID;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(username = "tester", password = "1")
@TestPropertySource("/application-test.properties")
@Sql(value = {"/create-user-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-post-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-post-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Sql(value = {"/create-user-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PostControllerTest {

	@Autowired
	private MockMvc mockMvc;
		
	@Test
	@Order(1)
	void updatePostTest() throws Exception{
		this.mockMvc.perform(post(MY_POSTS_UPDATE_URL)
				.param(PARAM_POST_ID, DEFAULT_NUMBER_ONE)
				.param(PARAM_POST_DESCRIPTION_UPDATE, PARAM_POST_UPDATE_VALUE)
				.param(PARAM_POST_TITLE_UPDATE, PARAM_POST_UPDATE_VALUE))
		.andDo(print())
		.andExpect(authenticated());
	}
	
	@Test
	@Order(2)
	void deletePostTest() throws Exception{
		this.mockMvc.perform(post(MY_POSTS_PAGE_URL)
				.param(PARAM_POST_ID, DEFAULT_NUMBER_ONE)
				.param(PARAM_ACTION_DELETE, PARAM_ACTION_DELETE_VALUE))
		.andDo(print())
		.andExpect(authenticated());
	}
	
	@Test
	@Order(3)
	void addPostTest() throws Exception {
		FileInputStream fileInputStream = new FileInputStream(new File(SOURCE_DEFAUL_IMAGE));
		MockMultipartFile mockMultipartFile = new MockMultipartFile(MOCK_MULTIPART_FILE_TYPE, MOCK_MULTIPART_FILE_NAME, MOCK_MULTIPART_FILE_FORM_DATA, fileInputStream);	
		this.mockMvc.perform(multipart(CREATE_POST_PAGE_URL)
				.file(mockMultipartFile)
				.param(PARAM_DESCRIPTION, PARAM_DESCRIPTION_VALUE)
				.param(PARAM_TITLE, PARAM_TITLE_VALUE)
				.param(PARAM_CATEGORY, PARAM_CATEGORY_VALUE)
				.param(PARAM_MEETING_TIME, PARAM_MEETING_TIME_VALUE)
				.param(PARAM_USER_ID, DEFAULT_NUMBER_ONE))
		.andDo(print())
		.andExpect(authenticated());
	}
}
