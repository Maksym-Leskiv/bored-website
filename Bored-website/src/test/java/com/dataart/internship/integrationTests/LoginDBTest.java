package com.dataart.internship.integrationTests;

import static com.dataart.internship.constans.DefaultAppTestConstans.ADMIN_PANEL_PAGE_URL;
import static com.dataart.internship.constans.DefaultAppTestConstans.USER_DETAILS_USER;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("/application-test.properties")
@Sql(value = { "/create-user-before.sql" }, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = { "/create-user-after.sql" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class LoginDBTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	@WithUserDetails(USER_DETAILS_USER)
	void accessSuccessfullyTest() throws Exception {
		this.mockMvc.perform(get(ADMIN_PANEL_PAGE_URL)).andDo(print()).andExpect(authenticated())
				.andExpect(status().is2xxSuccessful());
	}
}
