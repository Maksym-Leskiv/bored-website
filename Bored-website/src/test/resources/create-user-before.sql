
use boredtest;
SET SQL_SAFE_UPDATES = 0;
delete from user;

insert into user(id,email,password,role,username) values
(1,'admin@admin','$2a$10$QftdcYCHQX.MbXuSZjslPeAh8eZWUcuuC/WTB9UgOotKGl.dL8Fcy','ROLE_ADMIN','adminDB');